# Product Service for Wish Book app

`This repo is only tested with docker. The Vagrantfile should also work`

## Rules for Working with Git

1. Create a branch with your `name` or `bitbucket username`
2. Only develop in your branch
3. `One` commit = `One` bug fix only (avoid many changes in one commit)
4. Commit message should inform reader of what was fixed. Add JIRA bug number where possible
5. Make sure your branch is up-to-date with the latest `dev` branch
6. Test you code on your branch
7. Notify `Delivery lead` of your Change
    * Your branch will then merged into `dev`
    * Further tested
    * Finally the whole project will be released with a new version number. Using [SemVer](http://semver.org/)
    * Which will then be merged into master

## Docker

### To start the server with Docker

1. Install [Docker](https://www.docker.com/products/overview)
2. Make sure you can use the `docker-compose` command (```docker-compose version```)
3. Clone this repo
4. Change to cloned directory
5. There must be a network present called `wishbooknet`
    * To check if it exists use: ```docker network ls```
    * To create it if it doesn't exist use: ```docker network create wishbooknet```
6. Make sure that [CouchDb container](https://bitbucket.org/tigers-initium/ca.sears.wishbook.couchdbservice) is running on the same network
7. Run ```docker-compose up```

    The Node app is accessable at [http://localhost:8080](http://localhost:8080)

### To stop the container

1. Press `Ctrl+c`
2. Wait for containers to stop

    If containers don't stop quickly enough, press `Ctrl+c` again to force stop

## Vagrant

### To start the server with Vagrant

1. Install [Vagrant](https://www.vagrantup.com/downloads.html) for your system
2. Clone this repo
3. Change to cloned directory
4. Run ```vagrat up```

    The server should output its ip address (default is `192.168.33.11`) and the port the where the API is accessable.

#### To get in the Vagrant Box

1. Press `Ctrl+c` twice
2. Type `vagrant ssh` and hit enter
3. The project folder is located at `/vagrant`

### To stop the server

1. Press `Ctrl+c` twice
2. Then type `vagrant halt` and hit enter


## MISC

1. Globally install `eslint` for linting support
    * ```npm install --global eslint```
2. Add `editorconfig` support to your editor if not already present
    * [Atom](https://github.com/sindresorhus/atom-editorconfig)
    * [VS Code](https://github.com/editorconfig/editorconfig-vscode)
    * [Sublime Text](https://github.com/sindresorhus/editorconfig-sublime)
